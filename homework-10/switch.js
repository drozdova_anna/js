function switchTabs () {

    const tabs = document.querySelector(".tabs");

    tabs.addEventListener('click', (event) => {
        cleaner();

        event.target.classList.add("active");

        const characterText = document.querySelector(`#${event.target.innerHTML}-content`);
        characterText.classList.remove("hide");

    })
}

function cleaner () {
    const tabsArray = [...document.querySelectorAll(".tabs-title")];

    tabsArray.forEach( elem => {
        
        if (elem.classList.contains("active")){
            elem.classList.remove("active");
            document.querySelector(`#${elem.innerHTML}-content`).classList.add("hide");
        } 
    })
}


switchTabs();

