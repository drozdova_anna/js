function showArray ( array, parent = document.body ) {
    array.forEach(element => {
        if ( Array.isArray(element) ){      // обробкa вкладених масивів
            showArray(element, parent);
        } 
        else parent.insertAdjacentHTML("beforeend", `<li>${element}</li>`);
    });
}

function showNumber (number, node){
    node.innerHTML = number;
}

const arrayCity = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const array = ["1", "2", "3", "sea", "user", 23];

const wrapper = document.querySelector(".wrapper-list");
showArray(arrayCity, wrapper);
 



setTimeout(() => wrapper.remove(), 3000);

const timerDiv = document.querySelector(".timer");
setTimeout(() => showNumber(2, timerDiv), 1000);
setTimeout(() => showNumber(1, timerDiv), 2000);
setTimeout(() => showNumber(0, timerDiv), 3000);