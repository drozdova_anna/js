# 1
## Опишіть, як можна створити новий HTML тег на сторінці.
* document.createElement('[teg name]');
* Elem.innerHTML = "<div>New element</div>"

# 2
## Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
elem.insertAdjacentHTML(куди, html)
Перший параметр це кодове слово, яке вказує куди вставляти відносно elem

* "beforebegin" – вставити html перед elem,
* "afterbegin" – вставити html в elem, на початку,
* "beforeend" – вставити html в elem, в кінці,
* "afterend" – вставити html після elem.

# 3
## Як можна видалити елемент зі сторінки?
node.remove()