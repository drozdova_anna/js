# 1

## Document Object Model
Структура, которая формирует из элементов HTML объекты, к которым потом можно обращаться и взаимодействовать через js


# 2

## Яка різниця між властивостями HTML-елементів innerHTML та innerText?
* innerHTML 
  * Позволяет считывать и записывать не только текст, а и все теги, находящиеся внутри выбранного элемента
* innerText
  * Работает только с текстом


# 3
  
## Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

* elem.getElementsByTagName(tag)
* elem.getElementsByClassName(className)
* document.getElementsByName(name)
* document.getElementById
* document.querySelectorAl - Самый универсальный метод поиска
* querySelector - возвращает первый элемент, соответствующий данному CSS-селектору