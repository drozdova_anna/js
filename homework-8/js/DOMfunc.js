
changeParagraphBackground();
showElements("optionsList");
changeContent();
getElements();
// removeClass("section-title");
// there are no elements with class section-title in html code 
// but classes section-options and section-products were found
// removeClass("section-options");
removeClass("section-products");  //example


function changeParagraphBackground () {
    const paragraphs = document.querySelectorAll("p");
    if (paragraphs.length > 0){
        for (let i = 0; i < paragraphs.length; i++) {
            paragraphs[i].classList.add("paragraph-bg");
        }
    }
}

function showElements ( id ) {
    const listElem = document.getElementById(id);
    console.log(listElem);

    const father = listElem.parentElement;
    console.log(father);  //батьківський елемент

    const children = father.childNodes;  //дочірні ноди
    if (children.length > 0){
        for (let i = 0; i < children.length; i++) {
            console.log(`Node Type: ${children[i].nodeType} \n Node Name: ${children[i].nodeName}`)
        }
    } else {
        console.log("This Node doesn't have children");
    }
    
}

function changeContent () {
    const contentElem = document.querySelector("#testParagraph"); 
    // there are no elements with CLASS testParagraph in html code 
    // but id testParagraph was found
    // console.log(contentElem);
    contentElem.innerHTML = "This is a paragraph";
}

function getElements () {
    const mainHeader = document.querySelector(".main-header");
    const children = mainHeader.children;
    // console.log(children);

    if (children.length > 0){
        console.log("Children of an element with class main-header:");
        for (let i = 0; i < children.length; i++) {
            console.log(`${i} child: ${children[i]}`);
            children[i].classList.add("nav-item");
        }
    }
    return children;
}

function removeClass ( className ) {
    const elemsWithWrongClass = document.getElementsByClassName(className);
    for (let node of elemsWithWrongClass) {
        node.classList.remove(className);
    }
}