function btnListener () {
    const buttons = document.querySelectorAll(".btn");

    document.addEventListener( 'keyup', (event) => {
        buttons.forEach( btn => {
            if ( btn.innerHTML.toUpperCase() === event.key.toUpperCase() ){
                highlight(btn);
            }
        })
    } )
}

function highlight ( td ){
    if (selectedBtn){
        selectedBtn.classList.remove("btn-active");
    }
    selectedBtn = td;
    selectedBtn.classList.add("btn-active");
}


let selectedBtn = null;
btnListener();