        function isNumber(num) {
	        return typeof num === 'number' && !isNaN(num);
        }
        function calculate(variableOne, variableTwo, operation){
            if (operation === "+") return variableOne + variableTwo;
            if (operation === "-") return variableOne - variableTwo;
            if (operation === "/") return variableOne / variableTwo;
            if (operation === "*") return variableOne * variableTwo;
        }

        let userNumberOne, userNumberTwo, userOperation;

        userNumberOne = +prompt("enter the first number: ");
        while (!isNumber(userNumberOne)){
            userNumberOne = +prompt("enter the first number: ");
        }
        userNumberTwo = +prompt("enter the second number: ");
        while (!isNumber(userNumberTwo)){
            userNumberTwo = +prompt("enter the second number: ");
        }
        userOperation = prompt("What do you want to do with these values? ");

        console.log(calculate(userNumberOne, userNumberTwo, userOperation));
