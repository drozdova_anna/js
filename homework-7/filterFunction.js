// 1. Метод forEach проходится по всем items в массиве и проверяет их по заданному условию. 
//    Это callback функция. По механике похоже на цикл for.
// 2. Чтобы полностью очистить массив от элементов нужно установить его длину на ноль. 
//    Array.length = 0;
// 3. Метод Array.isArray() возвращает true, если объект является массивом 
//    и false, если он массивом не является.

function filterBy(array, typeOfData) {
    let filteredArray = array.filter(item => {
        return typeof(item) !== typeOfData;
    });
    return filteredArray;
}

const array =  ['hello', 'world', 23, '23', null];
const exception = 'string';
console.log(filterBy(array, exception));
