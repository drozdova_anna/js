function isNumber(num) {
    return typeof num === 'number' && !isNaN(num);
}

let userNumber = +prompt("enter the number: ");
while (!isNumber(userNumber)){
    userNumber = +prompt("enter the number: ");
}

for ( let i=0; i<=userNumber; i+=5 ){
    console.log(i);
}
if (userNumber<0) console.log("Sorry, no numbers");