function createNewUser(){
    const userData = prompt("введіть ваше ім'я та прізвище", "Олена Дроздова");
    const arrayUserData = userData.split(" ");
    const firstName = arrayUserData[0];
    const lastName = arrayUserData[1];

    const birthday = prompt("введіть ваш день народження", "17.03.1999");

    const newUser = {
        firstName,
        lastName,
        birthday,

        getLogin: function() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function(){
            let currentTime = new Date();
            let currentYear = +currentTime.getFullYear();
            let userAge = +this.birthday.slice(6);
            return currentYear - userAge;
        },
        getPassword: function(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        },
    };
    return newUser;
}

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());