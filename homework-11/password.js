function comparePasswords () {
    const firstPassword = document.querySelector(".first-password");
    const secondPassword = document.querySelector(".second-password");
    const errorText = document.querySelector(".error");

    document.querySelector("button").addEventListener("click", function(event){
        event.preventDefault();

        if ( firstPassword.value === secondPassword.value ){
            errorText.classList.add("error-hide");
            alert("You are welcome");
        } else {
            errorText.classList.remove("error-hide");
        }
    }, false)
}

function passwordVisibility () {
    document.querySelectorAll(".fas").forEach( icon => {

        icon.addEventListener("click", function(event){
            console.log(event.target.classList.contains("fas"));
            if (event.target.classList.contains("fa-eye")){
                event.target.classList.replace("fa-eye", "fa-eye-slash");
                event.target.previousElementSibling.type = "text";
            } else {
                event.target.classList.replace("fa-eye-slash", "fa-eye");
                event.target.previousElementSibling.type = "password";
            }
        }, false)
    })
}

comparePasswords();
passwordVisibility();
